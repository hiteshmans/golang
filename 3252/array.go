package main

import "fmt"

func main() {

	arr := [5]int{4, 5, 7}

	slice := []int{1, 2}

	fmt.Println(arr)
	fmt.Println(slice)
	slice = append(slice, 3)
	fmt.Println(slice)
	fmt.Println(slice[1:])
	fmt.Println(len(slice))

}
