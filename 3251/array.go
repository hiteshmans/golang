package main

import "fmt"

func main() {

	arr := [5]int{4, 5, 7}
	slice := []int{1, 2}

	fmt.Println(arr)
	fmt.Println(slice)

	slice = append(slice, 5)
	fmt.Println(slice)
	fmt.Println(slice[2:])
	fmt.Println(len(slice))

}
