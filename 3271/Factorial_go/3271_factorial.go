// Factorial Implementation using GO
package main

import "fmt"

func factorial(n int) int {
	if n == 0 {
		return 1
	} else {
		return n * factorial(n-1)
	}
}

func main() {

	i := 6

	result := factorial(i)

	fmt.Printf("The factorial of %d is %d\n", i, result)
}
