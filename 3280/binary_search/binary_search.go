// implementing binary search to search an element from an array

package main

import "fmt"

func binary_search(array []int, x int) int {

	var low, high = 0, len(array) - 1
	for low <= high {
		var mid = low + (high-low)/2
		if array[mid] == x {
			return mid
		}
		if x > array[mid] {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}
	return -1
}

func main() {
	arr := []int{1, 2, 9, 20, 31, 45, 63, 70, 100}
	x := 63
	bs := binary_search(arr, x)
	if bs != -1 {
		fmt.Println(x, "found in array at index", bs)
	} else {
		fmt.Println(x, "not found in array")
	}
}
