/*

this program is to implement fibonacci series i.e. 0 1 1 2 3 5 8...

*/

package main
import (
	"fmt"
)
//fibonacci
func fibonacci(n int) int{
	if n==0 || n==1 {
		return n
	}

	return fibonacci(n-1)+fibonacci(n-2)
}

func main(){
	var n int
	fmt.Print("Enter number: ")
	fmt.Scanln(&n)

	for i:=0;i<n;i++ {
		var fibi int=fibonacci(i)
		fmt.Print(fibi," ")
	}
	fmt.Println()
}